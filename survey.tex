% rubber: setlist arguments --shell-escape
\documentclass[12pt]{article}

\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}
\usepackage{mathpazo}
\usepackage[margin=1in]{geometry}

\begin{document}
\title{\textbf{Non-Native Grammar Correction}}
%\title{Native Language Identification and \\ Non-Native Grammar Correction}
\date{} % no date
\author{Sean Massung \\ \texttt{massung1}}
\maketitle

\section{Introduction}

Non-native speakers of English far outnumber native speakers; English is the
main language of books, newspapers, airports, air-traffic control, international
business, academic conferences, science, technology, diplomacy, sports,
international competitions, pop music, and advertising~\cite{british-council}.
Online education in the form of MOOCs (Massive Online Open Courses) is also
primarily in English. This creates enormous amounts of text written by
non-native speakers, which in turn generates a need for grammar correction and
analysis. While this survey primarily focuses on correcting and detecting
non-native \emph{English} text, most approaches are general and can be used
across any language pairing.

Dealing with non-native writing for educational purposes or NLP tasks usually
falls into one of the following broad categories:

\begin{itemize}
    \item Native language identification, a classification task
    \item Error identification and correction, usually determiner or preposition
        use
    \item Grammar correction, altering the original phrase structure
\end{itemize}

Clearly, the most general solution would be to correct the overall grammar; this
encompasses error identification and error correction at the lexical and
semantic level. Spelling correction~\cite{1992-spelling} is a potential parallel
task, though we choose to focus on grammar correction---both simple and complex.
Correcting speech is also an important issue: besides the obvious speech
recognition challenges in computer science and electrical engineering, speakers
tend to voice their words much differently than when writing. Therefore, we do
not include any investigation into these works in this paper.

This survey provides a structured overview of native language identification,
simple error identification and correction, and then explores existing methods
in grammatical structure correction using examples mainly drawn from the
CoNLL-2013 shared task. In each section, a figure briefly summarizes the papers
discussed and the methods used. If applicable, a comparison of performance is
also included.

\section{Identifying Native Language}

NLI is usually the first step in any second language error correction or author
profiling system. Identifying the native language of an anonymous text was first
popularized by Koppel~\cite{koppel2005} in 2005. Brooke~\cite{2012-robust-nli}
does an extensive survey of NLI feature efficacy, and develops a robust model
that works well when used across corpora. Before, NLI tasks were mostly
evaluated solely on a small learner corpus usually consisting of student essays.
It was previously thought that lexical features would be biased or overfit
towards essay topics, but the cross-corpus evaluation showed that this was not
the case.

The ICLE (International Corpus of Learners of English) corpus was an early
popular dataset to evaluate NLI tasks that contained five different European
languages. A table has been created (Fig~\ref{fig:nli-comp}) to portray the
summary described in~\cite{2012-robust-nli}. In addition to the results
displayed in the table, Wong and Dras also attempted to perform dimensionality
reduction with LDA~\cite{lda} as feature generation; however, this was not a
successful method.

\begin{figure}
  \begin{center}
    \begin{tabular}{|llll|}
        \hline
        \textbf{Paper} & \textbf{Year} & \textbf{Method} & \textbf{Accuracy} \\
        \hline
        Tsur and Rappoport~\cite{tsur} & 2007 & character $n$-grams & 66\% \\
        Wong and Dras~\cite{wong2009contrastive} & 2009 & syntactic errors & 74\% \\
        Wong and Dras~\cite{wong-rules} & 2011 & syntactic rules & 80\% \\
        Wong et.\ al.~\cite{wong-adaptors} & 2012 & adaptor grammars & 76\% \\
        Swanson and Charniak~\cite{swanson} & 2012 & tree substitution grammars & 78\% \\
        \hline
    \end{tabular}
    \caption{Summary of NLI results listed in Brooke~\cite{2012-robust-nli} for
    the ICLE corpus; accuracies added to chart.}
    \label{fig:nli-comp}
  \end{center}
\end{figure}

Tsur and Rappoport~\cite{tsur} discovered that incredibly simple top 200
frequent bigram character features fed to SVM led to $66\%$ accuracy on the five
native languages. They claim that word choice of non-native speakers is
influenced by the phonology of their native language (as evidenced by the
effectiveness of the character features). This is compared to a unigram words
baseline which achieved $47\%$ accuracy. They finally hypothesize that using a
spoken-language corpus would achieve even stronger results favoring character
bigrams since conscious effort put into speaking words is much less than writing
them.

Wong and Dras~\cite{wong2009contrastive} approach NLI through \emph{contrastive
analysis}: the idea that errors in text are influenced by the native language of
the author. They investigate three error types: subject-verb disagreement,
noun-number disagreement, and determiner misuse. These error types are then used
as ``stylistic markers'' for NLI features with an SVM classifier. To find these
errors in text, they use an open source grammar
checker\footnote{\url{http://queequeg.sourceforge.net/index-e.html}}.
Interestingly, ANOVA showed a measurable effect, but after combining their
contrastive features with existing methods, they were not able to significantly
increase the classification accuracy from Koppel~\cite{koppel2005}.

Wong and Dras~\cite{wong-rules} follow their previous work on contrastive
analysis, attempting to amend its shortcomings. Instead of error types, they use
two different features obtained from grammatical parse trees: ``horizontal
slices'' (production rules) and parse rerankings. They claim these are the first
pure syntactic features used in NLI\@. For the production rules, they
immediately applied information gain dimensionality reduction. The reranking
features are those contained in the Charniak parser and Stanford Parser trained
on the Wall Street Journal. Unlike the previous two attempts, the authors found
MaxEnt to outperform SVM as the classifier. Additionally, five-fold cross
validation was performed (as opposed to ten-fold), which brings to question
whether their accuracy can actually be compared with previous work. In any
event, they report a final accuracy of $80\%$, which is the highest reported as
of 2012.

Wong et.\ al.~\cite{wong-adaptors} explore the last author's---Mark
Johnson's---adaptor grammars~\cite{adaptor-grammars} to generate features.
Simply, adaptor grammars are a non-parametric extension to PCFGs (Probabilistic
Context Free Grammars). They can learn arbitrary-length word sequences; for
example, \emph{gradient descent} and \emph{cost function} were learned under a
machine learning topic. These adaptor grammars are used in two ways: in the
first, collocations are used as features in a MaxEnt classifier. In the second,
the grammar is trained on each class (representing native language). At test
time, the most probable grammar to have generated the text is selected. For both
tasks, the authors use five-fold cross validation on seven native languages. In
the feature-based classification, they achieved $76\%$; in the language
model-based classification, they achieved $50\%$.

Swanson and Charniak~\cite{swanson} made use of Tree Substitution Grammars
(TSGs)~\cite{tsgs}. Various tree induction methods are compared to generate
features, and five-fold cross validation on seven native languages is performed.
All TSG features outperformed the CFG baseline (at $73\%$). The highest TSG
induction method was Bayesian induction at $78\%$.

Massung et.\ al.~\cite{massung} also make use of grammatical parse tree
features, but mainly focus on their structural aspects as opposed to the
syntactic category labels. In one classification task, they found these features
to work well in determining the nationality of student essay writers from either
an English, Chinese, or Japanese background. These structural parse tree
features may be applicable in other tree-based objects such as adaptor grammars,
tree substitution grammars, and dependency parses.

Although not directly tackling NLI, Kim's authorship attribution
work~\cite{kim-2011} does use grammatical parse tree features similarly to the
above papers. They defined a new pattern, $k$-embedded-edge ($ee$) subtrees:
subtrees that share a set of $k$ ancestor-descendant subtrees. Therefore, a
0-$ee$ subtree would be one arbitrarily-sized subtree, and a 1-$ee$ subtree
would be one subtree and one descendant subtree anywhere in the parse tree. This
creates an exponential number of potential patterns, and the authors define
frequent pattern mining algorithms to prune the number of $ee$ tree features. As
with the last paper, this approach would be feasible for other tree structures.

\section{Correcting Simple Errors}

The next logical step after identifying the author's native language is to
correct any errors that are found. As Wong and Dras~\cite{wong2009contrastive}
introduced in the last section, contrastive analysis plays an important role in
NLI\@. They used an off-the-shelf grammar checker with performance that can
greatly be improved. As one would expect, the simplest errors to correct are
small insertions and deletions, largely ignoring the overall grammatical flow.
Article errors fit this simplistic definition perfectly; a correct sentence will
select the appropriate article from $\{a, an, the, \epsilon\}$, where $\epsilon$
represents ``no article necessary''. An even more straightforward version would
be to make the binary decision whether any article is necessary or not.

Easy examples of article misuse would be \emph{I want a food}, or \emph{I took
exam}. Deceptively simple at first, there are always more interesting cases:
\emph{He had beer} (or should it be \emph{He had a beer}?) and \emph{She was
paid by the hour} compared to \emph{They were going seventy miles an hour}. The
differences are quite subtle and at sometimes even colloquial---this can make
learning English extremely frustrating!

Automatically generating rules (or creating classifiers) is much preferred over
handcrafted heuristics primarily used in the 1980s and early 90s. The following
papers provide an overview of some of the existing techniques that don't require
human annotators. Fig~\ref{fig:error-comp} summarizes the papers reviewed for
article error.

\begin{figure}
  \begin{center}
    \begin{tabular}{|lll|}
       \hline
       \textbf{Paper} & \textbf{Year} & \textbf{Method} \\
       \hline
        Na-Re Han et.\ al.~\cite{2006-detecting} & 2006 & MaxEnt on NPs using
        unigram words and POS tags \\
        Hermet and D\'esilets~\cite{2009-correcting-prepositions} & 2009 &
        machine translation and syntactic pruning \\
        Gamon~\cite{2010-article-preposition} & 2010 & language model +
        classifier \\
        Rozovskaya and Roth~\cite{2010-paradigms} & 2010 & insert artificial
        errors into training data \\
       \hline
    \end{tabular}
    \caption{Comparison of article error techniques.}
    \label{fig:error-comp}
  \end{center}
\end{figure}

Na-Re Han et.\ al.~\cite{2006-detecting} train on a wide variety of corpora,
unlike previous works which trained on a single source such as the \emph{Wall
Street Journal}. They use fiction, non-fiction, and textbooks at all levels to
capture as much diversity as possible. The usual approach is to extract noun
phrases (NPs) and train on those; the same approach is taken here. Features used
were unigram words, part-of-speech (POS) tags, and positions to NP boundaries. A
MaxEnt model is used to predict the best label in the set $\{a, an, the,
\epsilon\}$.

In their dataset of NPs, the baseline to beat was $72\%$, in which the majority
class was $\epsilon$. Using unigram words and POS tags of all words in the NP,
they achieved $80\%$ accuracy. Using only the head POS tag, obtaining $77\%$ was
possible. They also applied their model to 668 human-annotated TOEFL essays by
native Chinese, Japanese, and Russian speakers. Their classifier had an $85\%$
agreement ($\kappa = 0.48$) with the human annotators. Again, the baseline was
rather high at $72\%$ as ``correct''. Despite this, these were the best results
published so far for article misuse with the added benefit of the absence of
handwritten rules.

Hermet and D\'esilets~\cite{2009-correcting-prepositions} investigated
correcting prepositional errors. Previous approaches only used a model of the
author's second language, though their method takes into account both the native
and non-native language. They attempt to capture whether the preposition errors
are produced by a misunderstanding of the second language, or a ``linguistic
inference'' from the first to second (\emph{i.e.} trying to translate too
literally). The machine translation technique comes into play on their Roundtrip
MT approach: a sentence is translated from $L2$ (second language) to $L1$
(native language), then back to $L2$. While fixing prepositions, it also
dramatically modified the sentences in some cases. Their second attempt involved
shallow parsing. They ran the parser on $L2$ sentences, and split the parses
around the erroneous prepositions. They then were able to prune (trim) these
fragments and connect them back together to form a coherent sentence.
Eventually, they found that neither of the methods outperformed the baseline,
but their combination was statistically significantly better.

Gamon~\cite{2010-article-preposition} combined a language model and classifier
into a ``meta-classifier'' that detected errors in both article and preposition
use. Additionally, they investigated how much more training data is needed for
the individual methods to approach the accuracy of the meta-classifier. Features
for the classifier were a window of six tokens to the right and left of errors,
POS tags, and lexical head features. The classifier was actually split into two
steps: first, determine the likely presence of a preposition or article. Then,
determine \emph{which} article or preposition should be chosen. The language
model is simply trained on LDC's \emph{Gigaword} corpus and log-likelihood was
used (normalized by sentence length). The meta-classifier uses features
generated from the two primary models such as ratio of likelihood scores from
the language model and classifier decisions. As expected, the meta-classifier
outperformed the two simpler models. Prepositions were harder to classify than
articles and they required more training data to reach a specific level of
accuracy. Future work would be including more primary models to feed features
into the meta-classifier.

Rozovskaya and Roth~\cite{2010-paradigms} took a different method towards
article error correction. Instead of training directly on non-native text, they
took native text and intentionally inserted article errors. In order to create a
realistic error distribution, they generated the article errors with the same
observed distribution as the non-native corpora. Advantages to training data
generation in this style are avoiding expensive data annotation, circumventing
small corpus sizes, and tailoring the classifier to a particular language. Most
of all, their method was shown to be superior to only training a classifier on
purely native data. One final note they make is that previous baselines used in
the literature are not always appropriate; baselines mentioned before are simply
the majority class. While this is acceptable for a selection task, this ignores
the actual error rate in the data. Usually, the error rate is much lower (shown
to be about 10\% in their experiments) than the majority class. Therefore, they
argue, a more fair comparison would be the \emph{error reduction}, and this is
indeed the measure they use to report their results. They reduced the error in
Chinese, Czech, and Russian writers by 10\%, 5\%, and 11\% respectively.

From this selection of work we see that correcting ``simple'' errors is usually
settled by training a classifier on the presence, absence, or specific
instantiation of articles or prepositions. Only Rozovskaya and
Roth~\cite{2010-paradigms} introduce a new, working paradigm. Hermet and
D\'esilets~\cite{2009-correcting-prepositions} introduce the novel Roundtrip
Machine Translation process, though it did not perform as well as hoped.

\section{Grammar Correction}

We now transition from non-native \emph{error} correction to non-native
\emph{grammar} correction. While we have seen the former tackled as a
classification task, the latter must be approached with a model capable of
capturing a deeper syntactic meaning.

Lee and Seneff~\cite{2006-agc} train a trigram language model on a lattice of
alternatives, where ``alternatives'' are prepositions, articles, and auxiliaries
that may or may not occur between words in the original text. For example, the
sentence \emph{I want flight Monday} can be corrected by inserting two tokens as
such: \emph{I want a flight on Monday}. Their algorithm first strips all such
alternatives from the original sentence. So far, this is not much different from
the article and preposition corrections. However, they additionally change each
remaining word in the input sentence to be a set of related words to the base
form: $want~\rightarrow~\{want, wants, wanted, wanting\}$. Their language model
then outputs the $k$-best candidates. These candidates are then given to a PCFG
and reranked. The final output is the top-ranked sentence from the PCFG\@.
Across all experiments, they found that reranking the language model candidates
significantly increased the $F$ measure.

Brockett et.\ al.~\cite{2006-smt} used statistical machine translation to
translate non-native speech into native speech. They first identified common
errors in a Chinese learner's English corpus, and used regular expressions to
convert target English from Reuters articles into ungrammatical English. This
approach is very similar to Rozovskaya and Roth's~\cite{2010-paradigms} error
insertion method, though instead of articles, it uses generic grammatical errors
such as \emph{I knew many informations about Christmas}. One does wonder,
however, how extensive or comprehensive the regular expressions were to
introduce grammatical errors (no examples are given or referenced).
Additionally, the regular expressions uniformly distribute errors throughout the
source language, which is not how errors naturally occur. Despite this, they did
find that their processed training data was able to be used in the MT system to
successfully correct errors. This shows that---given a source
model---statistical machine translation may be used to correct grammatical
errors.

West et.\ al.~\cite{2011-brw} uses bilingual random walks between $L1$ and $L2$
word senses. For example, on one side of the bipartite graph are the $L1$ words.
There are connections from a word $w\in L1$ to a word $w'\in L2$ if a $w$ could
be translated into $w'$. $w$ could be the English word ``head'', and be
translated into a physical head, head of an organization, or the verb ``to
head''. This model is used to correct non-native sounding phrases such as
\emph{entire stranger} to the more natural \emph{complete stranger}. This
bipartite graph is combined with a language model to correct non-native
sentences. In these experiments, the native language was Korean. Evaluation was
performed with Amazon Mechanical Turk workers choosing between the corrected
sentence and the original sentence. Results were not strongly positive, since
sometimes the corrected errors changed the meaning of the sentence or made it
ungrammatical. In future work the authors suggest using a richer language model
such as a PCFG.

\section{CoNLL-2013 Shared Task: Grammatical Error Correction}

We continue our discussion on grammatical error correction with the introduction
of the CoNLL-2013 shared task. The training data contained annotations
categorized by five error types, including the corrected text. Some systems
first classified potential errors by error type and then corrected errors while
others made no such distinction. Additionally, the teams received preprocessed
input: sentence segmentation, word tokenization, and POS tagging were all
performed.

In all, there are 1,397 essays consisting of 57,151 total sentences. Article and
determiner errors were most prevalent, with the other four classes appearing
roughly equally. Testing data was considerably smaller with only fifty essays.
Teams were also allowed to use any external resources that were non-proprietary
and publicly available. Fifty-four teams registered for the task, but only
seventeen submitted final results. Fig~\ref{fig:grammar-comp} shows the methods
of the top five teams. The five error types are listed below:

\begin{itemize}
    \item \textbf{Article or determiner}: ``\emph{In late nineteenth century,
        there was a severe air crash happening at Miami international
        airport.}'' Correction: replace \emph{late} with \emph{the late}.
    \item \textbf{Preposition}: ``\emph{Also tracking people is very dangerous
        if it has been controlled by bad men in a not good purpose.} Correction:
        replace \emph{in} with \emph{for}.
    \item \textbf{Noun number}: ``\emph{I think such powerful device shall not
        be made easily available.}'' Correction: replace \emph{device} with
        \emph{devices}.
    \item \textbf{Verb form}: ``\emph{However, it is an achievement as it is an
        indication that our society is progressed well and people are living in
        better conditions.}'' Correction: replace \emph{progressed} with
        \emph{progressing}.
    \item \textbf{Subject-verb agreement}: ``\emph{People still prefers to bear
        the risk and allow their pets to have maximum freedom.}'' Correction:
        replace \emph{prefers} with \emph{prefer}.
\end{itemize}

While the contest setup and evaluation does not completely mimic real-life
scenarios, it does provide useful information for those interested in
grammatical error correction. Since the errors are categorized into five groups,
it is possible to perform an error analysis on the results. On the other hand,
the categorization of non-native errors clearly requires a much more granular
measurement as well as additional classes.

As mentioned previously, even performing the evaluation is not a trivial task;
suggested corrections (while completely valid) may not actually match the
``official'' corrections. To mitigate these issues, the contest organizers
permit the contestants to submit their own gold standard corrections if they
disagreed with the provided answers. Of course, the corrections were reviewed.
It is also worth noting that the training and test data contained errors other
than those in the five categories, but these additional errors were not taken
into consideration in the final scoring.

\begin{figure}
  \begin{center}
      \begin{tabular}{|lllll|}
       \hline
       \textbf{Team} & \textbf{Method Summary} & $P$ & $R$ & $F_1$\\
       \hline
       UIUC~\cite{2013-task-uiuc} & averaged perceptron and Naive Bayes        & 46.45 & 23.49 & 31.20 \\
       NTHU~\cite{2013-task-nthu} & $n$-gram and dependency language model     & 23.80 & 26.35 & 25.01 \\
       HIT~\cite{2013-task-hit}   & MaxEnt and rules                           & 35.65 & 16.56 & 22.61 \\
       NARA~\cite{2013-task-nara} & phrase SMT and treelet language model      & 27.39 & 18.62 & 22.17 \\
       UMC~\cite{2013-task-umc}   & rule filter, MaxEnt, language model scorer & 28.49 & 17.53 & 21.70 \\
       \hline
      \end{tabular}
      \caption{Top five teams from the CoNLL-2013 shared task on grammatical
      error correction~\cite{2013-task-all}.}
      \label{fig:grammar-comp}
  \end{center}
\end{figure}

The fifth-place team, University of Macau (UMC)~\cite{2013-task-umc}, had a
separate module for each error type. First though, they ran a simple spelling
correction tool on the corpus. Additionally, they performed some preprocessing
to overcome the data sparsity problem (errors \emph{vs} non-errors were roughly
1:100). They referred to this as error label propagation: they simply used
$k$-NN to match labeled sentences with unlabeled ones. They then propagated the
errors from the source to the destination sentences by optimizing a loss
function based on the induced edges between sentences. This is perhaps the most
interesting contribution from their work. They added in some handmade rule-based
filters to select error types, and then shuttled each sentence through their
system. They used a language model scorer trained on the Google 5-gram corpus to
score their corrections. The Maximum Entropy classifier was only used in
detecting noun number and subject-verb agreement errors, where the rule-based
filters were used for all error types except the preposition errors. In the
preposition errors module, each preposition was simply replaced with those from
the set $\{in, for, to, of, on, \epsilon\}$ and the best was selected by the
highest-scoring according to the language model.

In fourth place, the Nara Institute of Science and Technology
(NARA)~\cite{2013-task-nara}, also took a hybrid approach depending on error
types. They ran each portion separately and then merged the modified sentences
together to form the final answer. To correct subject-verb agreement and verb
form, they used a treelet language model. A treelet refers to some subtree of an
entire parse tree. To correct noun number errors, they used a simple binary
classifier to distinguish between whether a noun should be singular or plural.
To correct preposition and determiner errors, they used phrase-based statistical
machine translation. The treelet language model is used to rank candidate
corrections. The candidates are generated by replacing verbs with a list of
potential verbs. For example, \emph{be} $\rightarrow\{be, being, been\}$. In the
machine translation unit, they generate candidate translations and have the
language model score the results in a similar way to the treelets.

The third-place team, Harbin Institute of Technology (HIT)~\cite{2013-task-hit},
split their system into two parts: a machine learning module (article \emph{vs}
determiner, prepositions, and noun errors) and a rule-based module (verb form
and subject-verb agreement). As indicated in the table, the classifier used was
a Maximum Entropy classifier with additional feature selection via genetic
algorithms and confidence tuning based on the Maximum Entropy score. The rules
were first preprocessed with the frequent pattern mining algorithm FP-growth to
find common incorrect phrases. These common phrases are then removed from the
candidate set to be corrected. A list of these common phrases is provided and
may prove useful for future work. In their results, they found that their models
were very sensitive to the parameters and that the confidence tuning had the
largest positive effect in their pipeline.

National Tsing Hua University (NTHU)~\cite{2013-task-nthu}, finished in second
place with a system that was very similar to theirs from last year. In the
CoNLL-2012 shared task, subject-verb agreement was not a labeled error type so
the NTHU system ignored these errors. In the seemingly current trend, a module
was created for each error type, though their modules were more similar to each
other than the previous systems. Each module used a moving window of words up to
length five---five here because they leverage the Google 5-gram corpus as well
as their previous tool Linggle\footnote{\url{http://linggle.com}}. Linggle is a
linguistic search engine in which the user can specify wildcards for nouns,
verbs, and adjectives~\cite{linggle}. Results are returned with those wildcards
filled in ordered by increasing likelihood in the dataset. These likelihoods are
used to score candidates generated by their system in each module. Their
candidate voting system is created using a backoff model of lower-order
$n$-grams. Verb form errors are really the only significantly different module
by including pointwise mutual information.

University of Illinois at Urbana-Champaign (UIUC)~\cite{2013-task-uiuc}, had the
best-performing system. They make use of previous work for article errors (with
averaged perceptron) and preposition errors (with Naive Bayes). The remaining
three error classes were dealt with Naive Bayes with priors trained on the same
Google corpus that NTHU used. Although the CoNLL corpus already contained POS
tags, they reparsed the dataset with their own tools in addition to running a
shallow parser for feature generation. There was no pipeline in the system,
meaning that corrections of each model were simply pooled together to create the
final output sentence. In their error analysis, they found that an incorrect
verb form can cause parsers to break, which greatly hinders rule-based methods.
Since the errors are so sparse, they hypothesize, simpler machine learning
algorithms would be more robust to the large amount of noise. This analysis
provides much insight and will be useful for future work.

After the contest ended, Rozovskaya and Roth~\cite{2013-joint} (from UIUC)
returned to the CoNLL-2013 shared tasks with a joint learning and inference
approach. Based on their observations from the shared task, they reason that
individual (read: \emph{independent}) classifiers per word do not capture
interactions between errors and word choice. They accomplish this by combining
individual classifiers using integer linear programming---a model which is able
to jointly learn the error occurrences. They thus increased the $F_1$ score on
the CoNLL-2013 data to $42\%$, significantly higher than their first place
score. They do note though, that $F_1$ may not be an appropriate measure; it is
indeed more intuitive to measure the increase in correctness of the original
data since it is quite likely that the grammar correction systems actually
introduce errors themselves.

\section{Conclusion}

We have provided a brief overview of non-native grammar correction starting with
the original problem of first identifying the author's native language. Then,
correcting simple errors such as article misuse were discussed. Several papers
describing actual \emph{grammar} correction were introduced, ending with results
and systems from the CoNLL-2013 shared task in grammar correction. We have also
explored different training paradigms and questioned evaluation measures
themselves.

\newpage

{\small
\bibliographystyle{plain}
\bibliography{bib}
}
\end{document}
